package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"reflect"
)

var (
	// ErrInternalServerError will throw if any the Internal Server Error happen
	ErrInternalServerError = errors.New("internal server error")
	// ErrNotFound will throw if the requested item is not exists
	ErrNotFound = errors.New("requested item is not found")
	// ErrConflict will throw if the current action already exists
	ErrConflict = errors.New("item already exist")
	// ErrBadParamInput will throw if the given request-body or params is not valid
	ErrBadParamInput = errors.New("given param is not valid")
	// ErrUnauthorized will throw if user give a wrong credential (password, access token)
	ErrUnauthorized = errors.New("wrong password/credential")
)

func FilterEmptyMap(m map[string]interface{}, l []string) map[string]interface{} {
	for key := range m {
		if !StringInSlice(key, l) {
			delete(m, key)
		}
	}

	return m
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func GetKeysStringFromMap(m interface{}) []string {
	keys := reflect.ValueOf(m).MapKeys()
	strKeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strKeys[i] = fmt.Sprint(keys[i])
	}
	return strKeys
}

func SetNewQueryParam(c echo.Context, new map[string]string) echo.Context {
	qp := c.QueryParams()
	for k, v := range new {
		qp.Set(k, v)
	}
	c.Request().URL.RawQuery = qp.Encode()

	return c
}

func PrettyPrint(v interface{}) string {
	pp, _ := json.MarshalIndent(v, "", "  ")
	return string(pp)
}

func BuildResponse(c echo.Context, isSuccess bool, status int, m string, err error, d interface{}) error {
	var result []byte
	if isSuccess {
		resSuccess := ResponseSuccess{
			Success: isSuccess,
			Message: m,
		}
		if d != nil {
			resWithData := ResponseSuccessWithData{
				ResponseSuccess: resSuccess,
				Data:            d,
			}

			result, _ = json.Marshal(resWithData)
		} else {
			result, _ = json.Marshal(resSuccess)
		}
	} else {
		res := ResponseError{
			Success:   isSuccess,
			Message:   err.Error(),
			ErrorCode: 0,
		}
		switch {
		case errors.Is(err, ErrInternalServerError):
			res.ErrorCode = 1000
			res.Message = ErrInternalServerError.Error()
			status = http.StatusInternalServerError
		case errors.Is(err, ErrNotFound) || errors.Is(err, gorm.ErrRecordNotFound):
			res.ErrorCode = 1002
			res.Message = ErrNotFound.Error()
			status = http.StatusNotFound
		case errors.Is(err, ErrConflict):
			res.ErrorCode = 1003
			res.Message = ErrConflict.Error()
			status = http.StatusConflict
		case errors.Is(err, ErrBadParamInput):
			res.ErrorCode = 1004
			res.Message = ErrBadParamInput.Error()
			status = http.StatusBadRequest
		case errors.Is(err, ErrUnauthorized):
			res.ErrorCode = 1005
			res.Message = ErrUnauthorized.Error()
			status = http.StatusUnauthorized
		default:
			res.ErrorCode = 1001
			res.Message = ErrInternalServerError.Error()
			status = http.StatusInternalServerError
		}
		result, _ = json.Marshal(res)
	}

	return c.JSONBlob(status, result)
}

type ResponseSuccess struct {
	Success bool   `json:"success" example:"true"`
	Message string `json:"message" example:"success get reservations"`
}

type ResponseSuccessWithData struct {
	ResponseSuccess
	Data interface{} `json:"data"`
}

type ResponseError struct {
	Success   bool   `json:"success" example:"false"`
	Message   string `json:"message" example:"your requested item is not found"`
	ErrorCode int    `json:"error_code" example:"1002"`
}

type DataWithPaging struct {
	Records interface{} `json:"records"`
	Next    string      `json:"next"`
	Prev    string      `json:"prev"`
}

type CustomValidator struct {
	Validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	err := cv.Validator.Struct(i)
	if err != nil {
		return err
	}
	return nil
}
